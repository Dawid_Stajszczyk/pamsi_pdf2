
#include "graf.h"

using namespace std;

/* Funkcja dodaje element do listy wierzchołków grafu */
int Graph::Add_Element(int i, int j, int wartosc)
{
	// i - z którego wierzchołka
	// j - do którego wierzchołka
	element = new List_Element;
	element->Vertex = j;
	element->Weigth = wartosc;
	element->Next = Graph_List[i];
	Graph_List[i] = element;
	return Graph_Matrix[i][j] = wartosc;
}

/* Funkcja wyświetlająca rozwiązanie problemu najkrótszych ścieżek w postaci macierzy. */
void Graph::Display()
{
	for (int i = 0; i < Number_Verticies; i++)
	{
		for (int j = 0; j < Number_Verticies; j++)
		{
			std::cout << Graph_Matrix[i][j] << "\t";
		}
		std::cout << endl;
	}
}


Graph::Graph(int Size)
{
	Number_Verticies = Size;
	Graph_Matrix = new int* [Number_Verticies];

	for (int i = 0; i < Number_Verticies; i++)
		Graph_Matrix[i] = new int[Number_Verticies];
	///////////////////////////////////////////////////////
	for (int i = 0; i < Number_Verticies; i++)
		for (int j = 0; j < Number_Verticies; j++)
			Graph_Matrix[i][j] = 0;
	//////////////////////////////////////////////////////////////
	Graph_List = new List_Element * [Number_Verticies];

	for (int i = 0; i < Number_Verticies; i++)
		Graph_List[i] = NULL;



}


/* Funkcja Relaxation_List() wykonuje relekasjację, czyli szuka najkrótszych dróg z wierzchołka startowego do wszystkich pozostałych. */
bool Graph::Relaxation_List(int start)
{
	int i, x;
	bool test;
	List_Element* pv;
	/* Koszty z wierzchołka startowego do jego samego wynosi 0. */
	d[start] = 0;
	/*pętla relaksacji */
	for (i = 1; i < Number_Verticies; i++)
	{
		test = true;
	/* Przechodzimy przez kolejne wierzchołki grafu. */
		for (x = 0; x < Number_Verticies; x++)
	/*Przeglądamy listę sąsiadów wierzchołka x.  */
			for (pv = Graph_List[x]; pv; pv = pv->Next)
	/* Sprawdzamy warunek relaksacji. */
				if (d[pv->Vertex] > d[x] + pv->Weigth)
				{
					/*Jeżeli obecny koszt drogi z wierzchołka startowego do x jest dłuższy niż droga
						od wierzchołka startowego do wierzchołka x prowadząca przez wierzchołek sąsiadujący to zamień te drogi. */
					test = false;
					d[pv->Vertex] = d[x] + pv->Weigth;
					p[pv->Vertex] = x;
				}
		if (test) return true;
	}
	/* Sprawdzamy czy istnieje ujemny cykl. */
	for (x = 0; x < Number_Verticies; x++)
		for (pv = Graph_List[x]; pv; pv = pv->Next)
			if (d[pv->Vertex] > d[x] + pv->Weigth)
				return false;

	return true;
}

/* Funkcja Relaxation_Matrix() wykonuje relekasjację, czyli szuka najkrótszych dróg z wierzchołka startowego do wszystkich pozostałych. */
bool Graph::Relaxation_Matrix(int start)
{
	int i, x;
	bool test;

	d[start] = 0;
	for (i = 1; i < Number_Verticies; i++)
	{
		test = true;
		for (x = 0; x < Number_Verticies; x++)
			for (int j = 0; j < Number_Verticies; j++)
				/* Jeżeli obecny koszt drogi z wierzchołka startowego do j-ego jest dłuższy niż droga
				od wierzchołka startowego do j-tego prowadząca przez wierchołek x to zamień te drogi. */
				if (d[j] > d[x] + Graph_Matrix[x][j] && Graph_Matrix[x][j] != 0)
				{
					/* false oznacza, że znaleziono krótszą droge. */
					test = false;
					d[j] = d[x] + Graph_Matrix[x][j];
					p[j] = x;
				}
		if (test) return true;
	}


	/* Sprawdzamy czy istnieje ujemny cykl. Gdy wykonano juz relaksację, a mimo tego znajdziemy
	jeszcze krótsze drogi to znaczy, że istnieje cykl ujemny.*/
	for (x = 0; x < Number_Verticies; x++)
		for (int j = 0; j < Number_Verticies; j++)
			if (d[j] > d[x] + Graph_Matrix[x][j] && Graph_Matrix[x][j] != 0) return false;

	return true;
}

/* Poniższa funkcja tworzy tablice d (koszty) i p (drogi). Tablice p ustawia na wartości ,,-1'',
a tablicę d na ,,nieskończoność''. Następnie wywoływana jest funkcja Relaxation_List(),
która wykonuje (wierzchołki-1) razy relaksację, znajdując tym samym najkrótsze drogi do
wszystkich wierzchołków    */
void Graph::BellmanFord_List() {

	int i, x, sptr, * S;
	bool End;
	d = new long long[Number_Verticies];
	p = new int[Number_Verticies];
	for (i = 0; i < Number_Verticies; i++)
	{
		d[i] = INT_MAX;
		p[i] = -1;
	}

	End = Relaxation_List(start);

	/* Jeśli znaleziono wszystkie drogi (End ==True) to do pliku Results_List
	zapisujemy kolejne wierzchołki, najkrótszą drogę do nich z wierzchołka
	startowego oraz koszt tej drogi. */ 

	if (End)
	{
		/* Tworzymy stos, w którym będziemy przechowywać wierzchołki. */
		S = new int[Number_Verticies];
		sptr = 0;


		ofstream FILE;
		FILE.open("Results_List.txt");
		for (i = 0; i < Number_Verticies; i++)
		{
			FILE << i << ": ";
			for (x = i; x != -1; x = p[x])
				S[sptr++] = x;

			while (sptr)
				FILE << S[--sptr] << " ";

			FILE << "Costs: " << d[i] << endl;
		}

		delete[] S;
	}
	/* Jeśli wystąpi ujemny cykl to nic nie zapisuj i zgłoś komunikat o tym.*/
	else cout << "Ujemny cykl" << endl;
}

/* Funkcja BellmanFord_Matrix() jest analogiczna do funkcji BellmanFord_List().
Jedyna różnica jest w funkcji Relaxation_Matrix(). Tam relaksacja jest wykonywana na tablicy, a nie na liście.*/

void Graph::BellmanFord_Matrix() {

	int i, x, sptr, * S;
	bool End;
	d = new long long[Number_Verticies];
	p = new int[Number_Verticies];
	for (i = 0; i < Number_Verticies; i++)
	{
		d[i] = INT_MAX;
		p[i] = -1;
	}
	cout << endl;
	End = Relaxation_Matrix(start);

	if (End)
	{
		S = new int[Number_Verticies];
		sptr = 0;


		ofstream FILE;
		FILE.open("Results_Matrix.txt");
		for (i = 0; i < Number_Verticies; i++)
		{
			FILE << i << ": ";
			for (x = i; x != -1; x = p[x])
				S[sptr++] = x;

			while (sptr)
				FILE << S[--sptr] << " ";

			FILE << "koszt : " << d[i] << endl;
		}

		delete[] S;
	}
	else cout << "Ujemny cykl" << endl;
}

/* Funkcja wyświetlająca rozwiązanie problemu najkrótszych ścieżek w postaci listy sąsiedztwa. */
void Graph::Display_List()
{
	/* Zmienna Auxiliary będzie wskazywać na element z listy sąsiedztwa */
	List_Element* Auxiliary;
	for (int i = 0; i < Number_Verticies; i++)
	{
		cout << "L[" << i << "] =";
		/* Będziemy wypisywać wagi z wierzchołka i-tego do pozostałych wierzchołków z list sąsiedztwa. */
		Auxiliary = Graph_List[i];
		while (Auxiliary)
		{
			cout << Auxiliary->Vertex << "(" << Auxiliary->Weigth << ") ";
			Auxiliary = Auxiliary->Next;
		}
		cout << endl;
	}
}



Graph::~Graph()
{
	for (int i = 0; i < Number_Verticies; i++)
		delete[] Graph_Matrix[i];
	delete[] Graph_Matrix;

	delete[] Graph_List;
}

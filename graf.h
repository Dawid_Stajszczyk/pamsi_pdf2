
#pragma once
#include <iostream>
#include <fstream>

using namespace std;

struct List_Element {
	List_Element* Next;
	int Vertex;
	int Weigth;
};

class Graph
{
public:
	/* Konstruktor, który tworzy graf, który posiada 1 wierzchołek, gdy nie podamy argumentu. */
	Graph(int Size = 1);
	/* Destruktor */
	~Graph();
	/* Dodanie elementu do listy i do macierzy sąsiedztwa,  */
	int Add_Element(int i, int j, int wartosc);
	/* Funkcje wyświetlające zawartości macierzy oraz listy (zawierają wagi pomiędzy wszystkimi parami wierzchołków) */
	void Display();
	void Display_List();

	/* Algortytm Bellmana Forda na liście, przy czym funkcja Relaxation_List wykonuje główną część algorytmu - relaksację */
	void BellmanFord_List();
	bool Relaxation_List(int start);

	/* Analogiczne funkcje do dwóch powyższych tylko na tablicach. */
	void BellmanFord_Matrix();
	bool Relaxation_Matrix(int start);

	/* Przypisz wierzchołek startowy do obiektu */
	void First_Vertex(int start) { this->start = start; }


private:
	/* Wskazuje na drogę  */
	int* p;
	/* Wskazuje na koszty drogi */
	long long* d;
	/* Do utworzenia elementu na liście grafu. */
	List_Element* element;
	/* Do utworzenia listy grafu */
	List_Element** Graph_List;
	/* Do utworzenia macierzy grafu */
	int** Graph_Matrix;
	/* Liczba wierzchołków */
	int Number_Verticies;
	/* Liczba krawędzi */
	int Number_Edges;
	/* Wierzchołek startowy */
	int start;
};





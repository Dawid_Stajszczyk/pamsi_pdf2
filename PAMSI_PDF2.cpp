

#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include "graf.h"

using namespace std;

///////////////////////////////////////////
/* Funkcja Load wczytuje z pliku tekstowego dane
do Grafu i wypełnia nimi atrybuty obiektu klasy Graph */
Graph* Load(Graph* New)
{
	int Number_Edges;
	int Number_Verticies;
	int Beggining;
	int A_Vertex;
	int B_Vertex;
	int Weigth;
	string Name_FILE;
	ifstream FILE;
	cout << "Podaj nazwe pliku" << endl;
	cin >> Name_FILE;

	/* Otwieramy plik tekstowy z danymi do grafu. */
	FILE.open(Name_FILE.c_str());

	if (FILE.good())
	{
		FILE >> Number_Edges;
		FILE >> Number_Verticies;
		FILE >> Beggining;
		/* Tworzymy obiekt klasy Graph o zadanej liczbie wierzchołków */
		New = new Graph(Number_Verticies);
		/* Oraz nadajemy wierzchołek startowy*/
		New->First_Vertex(Beggining);
		while (!FILE.eof())
		{
			/* Pobieramy kolejne wiersze z pliku tekstowego, a następnie przypisujemy do do odpowiednich atrybutów obiektu klasy Graph */
			FILE >> A_Vertex;
			FILE >> B_Vertex;
			FILE >> Weigth;

			New->Add_Element(A_Vertex, B_Vertex, Weigth);
		}

	}
	else
	{
		cout << "Nieprawidlowa nazwa pliku" << endl;
	}
	FILE.close();
	return New;

}

// Funkcja Generation tworzy nam plik tekstowy z danymi do grafu tj.
/////////////// 1 wiersz //////////////////////////////////////
// ilość krawędzi ilość_wierzchołków wierzchołek startowy /////
///////////////// Kolejne wiersze ////////////////////////////
// wierzchołek początkowy wierzchołek końcowy Waga  /////////

void Generation()
{
	int Number_Edges;
	int Number_Verticies;
	int Beggining;

	string Name_FILE;
	ofstream FILE;
	int Weigth;
	double Density;
	cout << "Podaj nazwe pliku" <<endl;
	cin >> Name_FILE;

	/*Tworzymy plik tekstowy o podanej nazwie */
	FILE.open(Name_FILE.c_str());
	cout << " Podaj liczbe wierzcholkow oraz wierzcholek startowy: \n";
	cin >> Number_Verticies >> Beggining;
	cout << " Podaj gestosc grafu: " <<endl;
	cin >> Density;
	/* Wzór na liczbę krawędzi w grafie pełnym */
	Number_Edges = (Number_Verticies * (Number_Verticies - 1)) / 2;
	/* Wybieramy gęstość grafu */
	Number_Edges = static_cast<int>(Number_Edges * Density);
	if (FILE.good())
	{

		FILE << Number_Edges << " " << Number_Verticies << " " << Beggining << endl;

		for (int i = 0; i < Number_Verticies; i++)
			for (int j = i; j < Number_Verticies; j++)
				if (i != j)
				{
					/* waga jest przypisywana losowo z przedzialu od 1 do 50 */
					Weigth = rand() % 50 + 1;
					/*Należy wypisać wszystkie pary liczb uwzględniając graf skierowany, a więc droga A->B nie jest tożsama z drogą B->A */
					FILE << i << " " << j << " " << Weigth << endl;
					FILE << j << " " << i << " " << Weigth << endl;
				}
	}
	else
	{
		cout<<"Nieprawidlowa nazwa pliku"<<endl;
	}
	FILE.close();

}



int main()
{

	/* ofstream - klasa obsługująca wyjście FILEu */
	ofstream FILE;
	/* tworzymy plik tekstowy z czasami wykonywania */
	FILE.open("Results_Time.txt", ios::app);
	unsigned int Option;
	int Number_Verticies = 1;
	/* Tworzymy wskaźnik na nasz graf */
	Graph* New = NULL;
	/* Tablica, która przechouje nam droga od zerowego do i-tego wierzchołka */
	int* Road_array = new int[Number_Verticies];
	while (1)
	{

		cout << "1 - Generacja" << endl;
		cout << "2 - Wczytywanie pliku" << endl;
		cout << "3 - Algorytm Bellmana Forda lista" << endl;
		cout << "4 - Algorytm Bellmana Forda macierz" << endl;
		cout << "5 - Wyswietlanie danych" << endl;
		cout << "0 - Koniec programu" << endl;

		cout << "Wybierz jedna z dostepnych opcji:" << endl;

		cin >> Option;

		switch (Option)
		{

		case 1:
			Generation();
			break;
		case 2:
			New = Load(New);
			break;
		case 3:
		{
			clock_t start = clock();
			New->BellmanFord_List();
			FILE << "Graf jako lista " << clock() - start << endl;
		}
		break;
		case 4:
		{
			clock_t start = clock();
			New->BellmanFord_Matrix();
			FILE << "Graf jako macierz " << clock() - start << endl;
		}
		break;
		case 5:
			if (New == NULL)
				cout << "Brak tresci do wyswietlenia" << endl;
			else
			{
				New->Display();

				cout << endl << endl;
				New->Display_List();
			}

			break;

		case 0: return 0;

		default:
			cout << "Nieprawidłowy klawisz" << endl;
			break;
		}
	}
}
